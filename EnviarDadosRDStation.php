<?php
namespace Rubeus\IntegracaoRDStation;
use Rubeus\ContenerDependencia\Conteiner;

class EnviarDadosRDStation {
    private $apiInserirLead;
    private $apiAlterarEtapaLead;
    private $apiInformarVendaPerda;

    function __construct()
    {
        $linkBase = Conteiner::get('ConsultaApp')->consultarLinkApi(1);
        $this->apiInserirLead = "{$linkBase}conversions";
        $this->apiAlterarEtapaLead = "{$linkBase}funnels";
        $this->apiInformarVendaPerda = "{$linkBase}sales";

    }

    private function enviar($dados, $url, $metodo=false){
        $gerenciador = new \crm\Geral\Aplicacao\Servico\GerenciarToken(2, 1);
        $token = $gerenciador->getTokenSemUser();
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        if($metodo){
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $metodo);
        }
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($dados));
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: Bearer ' . $token
        ));

        $responseText = curl_exec($curl);
        curl_close($curl);
        Conteiner::registrar('dadosEnviadoRDStation', json_encode($dados));
        Conteiner::registrar('retornoRDStation', $responseText);
        return $responseText;
    }

    public function inserirLead($dados){
        $envio = json_decode($this->enviar($dados, $this->apiInserirLead));
        Conteiner::registrar('enviadoRDStation', $envio && $envio->result == 'success' ? 1 : 0);
    }

    public function mudarEtapaLead($dados){
        $envio = $this->enviar($dados, $this->apiAlterarEtapaLead, 'PUT');
        Conteiner::registrar('enviadoRDStation', $envio == '{}' ? 1 : 0);
    }

    public function informarVendaPerda($dados){
        $envio = $this->enviar($dados, $this->apiInformarVendaPerda);
        Conteiner::registrar('enviadoRDStation', $envio == '{}' ? 1 : 0);
    }
}
