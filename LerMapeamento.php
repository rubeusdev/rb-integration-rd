<?php
namespace Rubeus\IntegracaoRDStation;

class LerMapeamento{
    private $enderecoMapaXml = DIR_BASE.RDSTATION_MAPEAMENTO_CAMPOS;
    private $mapa;

    public function getMapa(){
        if(!$this->mapa){
            $this->mapa = simplexml_load_file($this->enderecoMapaXml);
        }
        return $this->mapa;
    }
}
