<?php

namespace Rubeus\IntegracaoRDStation;

use Rubeus\IntegracaoRDStation\EnviarDadosRDStation;
use Rubeus\IntegracaoRDStation\LerMapeamento;


class IntegracaoRDStation
{
    private $lerMapeamento;
    private $enviarDadosRDStation;

    public function __construct()
    {
        $this->lerMapeamento = new LerMapeamento();
        $this->enviarDadosRDStation = new EnviarDadosRDStation();
    }

    public function enviarNovoLead($dados, $eventoIdentificador, $tags = '')
    {
        $lead =   new \stdClass();
        $lead->identificador = $eventoIdentificador;
        $lead->tags = $tags;
        $this->popularDemaisInformacoes($lead, $dados);
        $this->enviarDadosRDStation->inserirLead($lead);
    }

    private function popularDemaisInformacoes($lead, $dados)
    {
        $mapa = $this->lerMapeamento->getMapa();
        for ($i = 0; $i < count($mapa->Lead->propriedade); $i++) {
            $lead->{rtrim($mapa->Lead->propriedade[$i]['rd'])} =  $dados[rtrim($mapa->Lead->propriedade[$i]['rubeus'])];
        }
    }

    public function mudarEtapaLead($email, $posicao, $oportunidade = false)
    {
        $lead = new \stdClass();
        $lead->email = $email;
        $lead->payload = new \stdClass();
        $lead->payload->lifecycle_stage = 'Qualified Lead';
        $lead->payload->opportunity = $oportunidade;
        $this->enviarDadosRDStation->mudarEtapaLead($lead);
    }

    public function informarVenda($email, $valor, $leadId = false)
    {
        $venda = new \stdClass();
        $venda->event_type = 'SALE';
        $venda->event_family = 'CDP';
        $venda->payload = new \stdClass();
        $venda->payload->email = $email;
        $venda->payload->value = $valor;
        $venda->payload->funnel_name = "default";
        $this->enviarDadosRDStation->informarVendaPerda($venda);
    }

    public function informarPerda($email, $motivo)
    {
        $perda = new \stdClass();
        $perda->event_type = 'OPPORTUNITY_LOST';
        $perda->event_family = 'CDP';
        $perda->payload = new \stdClass();
        $perda->payload->email = $email;
        $perda->payload->reason = $motivo;
        $perda->payload->funnel_name = "default";
        $this->enviarDadosRDStation->informarVendaPerda($perda);
    }
}
