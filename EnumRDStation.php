<?php
namespace Rubeus\IntegracaoRDStation;

abstract class EnumRDStation {
    const LEAD = 0;
    const LEAD_QUALIFICADO = 1;
    const CLIENTE = 2;

    const VENDA = 'SALE';
    const PERDIDO = 'OPPORTUNITY_LOST';
}
